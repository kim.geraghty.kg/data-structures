# Write your code here to make the tests pass.
#
# Change your working directory to this directory,
# linked_queue.
#
# # Start by running python -m pytest tests/test_01.py and
# making the test pass.
#
# Then, run python -m pytest tests/test_02.py to make the
# next test pass. Keep going to tests/test_15.py.

class LinkedQueue:

  def __init__(self, head=None, tail=None):
    self.head = head
    self.tail = tail

  def dequeue(self):
    # empty queue:
    if self.head is None and self.tail is None:
      raise Exception

    value = self.head.value

    # reassign head property to node's link
    self.head = self.head.link

    # if final item is removed:
    if self.head is None:
      self.tail = None

    return value

  def enqueue(self, val):

    n = LinkedQueueNode(val)

    # if there is no head yet, aka queue is empty: add Node
    if self.head is None:
      self.head = n
      self.tail = n

    else:
      # link for old tail becomes new node
      self.tail.link = n
      # tail points to new tail
      self.tail = n



queue = LinkedQueue()

class LinkedQueueNode:

  def __init__(self, value=None, link=None):
    self.value = value
    self.link = link


node5 = LinkedQueueNode(5)
node10 = LinkedQueueNode(10)
node50 = LinkedQueueNode(50)

queue.enqueue(node5)
queue.enqueue(node10)
queue.enqueue(node50)

queue.dequeue()
